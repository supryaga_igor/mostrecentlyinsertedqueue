package com.supryaga;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class MostRecentlyInsertedBlockingQueueTest {
    private MostRecentlyInsertedBlockingQueue<Integer> mostRecentlyInsertedBlockingQueue;
    private final int QUEUE_SIZE = 3;

    @Before
    public void setUp() {
        this.mostRecentlyInsertedBlockingQueue =
                new MostRecentlyInsertedBlockingQueue(QUEUE_SIZE);
    }

    @Test
    public void threadSafetyTest() throws InterruptedException {
        MostRecentlyInsertedBlockingQueueThread mostRecentlyInsertedQueueThread1 =
                new MostRecentlyInsertedBlockingQueueThread(mostRecentlyInsertedBlockingQueue, 3, true);
        MostRecentlyInsertedBlockingQueueThread mostRecentlyInsertedQueueThread2 =
                new MostRecentlyInsertedBlockingQueueThread(mostRecentlyInsertedBlockingQueue, 4, true);
        MostRecentlyInsertedBlockingQueueThread mostRecentlyInsertedQueueThread3 =
                new MostRecentlyInsertedBlockingQueueThread(mostRecentlyInsertedBlockingQueue, 4, false);
        mostRecentlyInsertedQueueThread1.start();
        mostRecentlyInsertedQueueThread2.start();
        mostRecentlyInsertedQueueThread3.start();
        mostRecentlyInsertedQueueThread1.join();
        mostRecentlyInsertedQueueThread2.join();
        mostRecentlyInsertedQueueThread3.join();

        assertEquals(QUEUE_SIZE, mostRecentlyInsertedBlockingQueue.size());
    }
}
