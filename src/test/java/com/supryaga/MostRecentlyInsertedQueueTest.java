package com.supryaga;

import org.junit.Before;
import org.junit.Test;

import java.util.Queue;
import static org.junit.Assert.assertEquals;

public class MostRecentlyInsertedQueueTest {

    private Queue mostRecentlyInsertedQueue;
    private final int QUEUE_SIZE = 3;

    @Before
    public void setUp() {
        mostRecentlyInsertedQueue = new MostRecentlyInsertedQueue(QUEUE_SIZE);
    }

    @Test
    public void sizeBound() {
        for (int i = 0; i < 10; i++) {
            mostRecentlyInsertedQueue.offer(i);
        }
        assertEquals(QUEUE_SIZE, mostRecentlyInsertedQueue.size());
    }

    @Test
    public void newElementsAddedToTheTail() {
        mostRecentlyInsertedQueue.offer(1423);
        mostRecentlyInsertedQueue.offer(0);
        mostRecentlyInsertedQueue.offer(-10);
        assertEquals(1423, mostRecentlyInsertedQueue.peek());
    }
}
