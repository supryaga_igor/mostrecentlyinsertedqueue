package com.supryaga;

public class MostRecentlyInsertedBlockingQueueThread extends Thread {
    private MostRecentlyInsertedBlockingQueue<Integer> mostRecentlyInsertedBlockingQueue;
    private int multiplier;
    private boolean insert;

    public MostRecentlyInsertedBlockingQueueThread(MostRecentlyInsertedBlockingQueue<Integer> mostRecentlyInsertedBlockingQueue,
                                                   int multiplier, boolean insert) {
        this.mostRecentlyInsertedBlockingQueue = mostRecentlyInsertedBlockingQueue;
        this.multiplier = multiplier;
        this.insert = insert;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 3; i++) {
                if (insert) {
                    mostRecentlyInsertedBlockingQueue.enqueue(i * multiplier);
                } else {
                    mostRecentlyInsertedBlockingQueue.dequeue();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
