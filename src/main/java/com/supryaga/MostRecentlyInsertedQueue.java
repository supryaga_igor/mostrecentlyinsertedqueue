package com.supryaga;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.LinkedList;

public class MostRecentlyInsertedQueue<T> extends AbstractQueue<T> {

    private int sizeLimit;
    private LinkedList<T> elementsStorage;

    public MostRecentlyInsertedQueue(int sizeLimit) {
        this.sizeLimit = sizeLimit;
        this.elementsStorage = new LinkedList<T>();
    }

    public Iterator<T> iterator() {
        return elementsStorage.iterator();
    }

    public int size() {
        return elementsStorage.size();
    }

    public boolean offer(T t) {
        if (t == null) {
            throw new NullPointerException("Should be a non-nullable value");
        }
        if (elementsStorage.size() < sizeLimit) {
            elementsStorage.add(t);
        } else {
            elementsStorage.poll();
            elementsStorage.add(t);
        }
        return true;
    }

    public T poll() {
        T element = elementsStorage.peek();
        elementsStorage.poll();

        return element;
    }

    public T peek() {
        return elementsStorage.getFirst();
    }
}
