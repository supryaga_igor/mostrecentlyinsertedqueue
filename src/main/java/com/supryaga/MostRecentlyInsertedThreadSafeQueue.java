package com.supryaga;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

public class MostRecentlyInsertedThreadSafeQueue<T> extends AbstractQueue<T> {

    private int sizeLimit;
    private LinkedList<T> elementsStorage;
    private ReentrantLock reentrantLock;

    public MostRecentlyInsertedThreadSafeQueue(int sizeLimit) {
        this.sizeLimit = sizeLimit;
        this.elementsStorage = new LinkedList<T>();
        this.reentrantLock = new ReentrantLock();
    }

    public Iterator<T> iterator() {
        LinkedList<T> tempStorage = new LinkedList<T>(elementsStorage);

        return tempStorage.iterator();
    }

    public int size() {
        reentrantLock.lock();
        try {
            return elementsStorage.size();
        } finally {
            reentrantLock.unlock();
        }
    }

    public boolean offer(T t) {
        if (t == null) {
            throw new NullPointerException("Should be a non-nullable value");
        }
        reentrantLock.lock();
        try {
            if (elementsStorage.size() < sizeLimit) {
                elementsStorage.add(t);
            } else {
                elementsStorage.poll();
                elementsStorage.add(t);
            }
            return true;
        } finally {
            reentrantLock.unlock();
        }
    }

    public T poll() {
        reentrantLock.lock();
        try {
            T element = elementsStorage.peek();
            elementsStorage.poll();

            return element;
        } finally {
            reentrantLock.unlock();
        }
    }

    public T peek() {
        reentrantLock.lock();
        try {
            return elementsStorage.getFirst();
        } finally {
            reentrantLock.unlock();
        }
    }
}
