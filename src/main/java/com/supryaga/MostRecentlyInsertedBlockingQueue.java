package com.supryaga;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.LinkedList;

public class MostRecentlyInsertedBlockingQueue<T> extends AbstractQueue<T> {
    private int sizeLimit;
    private LinkedList<T> elementsStorage;

    public MostRecentlyInsertedBlockingQueue(int sizeLimit) {
        this.sizeLimit = sizeLimit;
        this.elementsStorage = new LinkedList<T>();
    }

    public Iterator<T> iterator() {
        return elementsStorage.iterator();
    }

    public synchronized int size() {
        return elementsStorage.size();
    }

    public synchronized boolean offer(T t) {
        if (t == null) {
            throw new NullPointerException("Should be a non-nullable value");
        }
        if (elementsStorage.size() < sizeLimit) {
            elementsStorage.add(t);
        } else {
            elementsStorage.poll();
            elementsStorage.add(t);
        }
        return true;
    }

    public synchronized T poll() {
        T element = elementsStorage.peek();
        elementsStorage.poll();

        return element;
    }

    public synchronized T peek() {
        return elementsStorage.getFirst();
    }

    public synchronized void enqueue(T item) throws InterruptedException {
        while (this.elementsStorage.size() == this.sizeLimit) {
            wait();
        }
        if (this.elementsStorage.size() == 0) {
            notifyAll();
        }
        this.elementsStorage.add(item);
    }

    public synchronized T dequeue() throws InterruptedException {
        while (this.elementsStorage.size() == 0) {
            wait();
        }
        if (this.elementsStorage.size() == this.sizeLimit) {
            notifyAll();
        }

        return this.elementsStorage.remove(0);
    }
}
